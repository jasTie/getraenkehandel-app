﻿using GetraenkeMarkt.Classes;
using GetraenkeMarkt.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace GetraenkeMarkt.Menu
{
    public class MainMenu : AddDrink
    {


        private static string mainMenu = "Willkommen in der Getränkeerfassung!\n" +
            "Bitte wählen Sie eine Aktion:\n" +
            "1. Neues Getränk erfassen\n" +
            "2. Alle erfassten Getränke auflisten\n" +
            "3. Getränk löschen\n" +
            "4. Programm beenden\n" +
            "Tragen Sie hier die Nummer der Aktion ein: ";

        List<Getraenke> getraenkeliste = new List<Getraenke>();

        public void Start()
        {
            

            Console.WriteLine(mainMenu);
            var action = Console.ReadLine();
            MoveToChoosenAction(action, getraenkeliste);      
        }

        private void MoveToChoosenAction(string action, List<Getraenke> getraenkeListe)
        {
            Console.Clear();
            ShowDrinks _showDrinks = new ShowDrinks();
            DeleteGetraenke _deleteGetraenk = new DeleteGetraenke();

            while (action != "4")
            {
                switch (action)
                {
                    case "1":
                        CreateAndAddDrink(getraenkeListe);
                        break;
                    case "2":
                        _showDrinks.ShowList(getraenkeListe);
                        break;
                    case "3":
                        _deleteGetraenk.DeleteDrinks(getraenkeListe);
                        break;
                }
                Start();
            }
            CloseApplication();
        }

        private void CloseApplication()
        {
            Environment.Exit(0);
        }
    }
}
