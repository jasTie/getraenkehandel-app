﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetraenkeMarkt.Classes
{
    public class Getraenke
    {
            public string Name { get; set; }
            public string Anzahl { get; set; }
            public string Lagerort { get; set; }
    }
}
