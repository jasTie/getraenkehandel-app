﻿using GetraenkeMarkt.Menu;
using System;
using System.Threading;

namespace GetraenkeMarkt
{
    class Program
    {
        static void Main(string[] args)
        {
            MainMenu _mainMenu = new MainMenu();
            _mainMenu.Start();
        }
    }
}
