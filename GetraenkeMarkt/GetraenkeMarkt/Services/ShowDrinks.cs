﻿sing GetraenkeMarkt.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GetraenkeMarkt.Services
{
    public class ShowDrinks
    {
        public void ShowList(List<Getraenke> getraenkeList)
        {
            Console.Clear();
            ShowGetraenke(getraenkeList);
            Console.ReadLine();
        }

        public void ShowGetraenke(List<Getraenke> getraenkeList)
        {
            for (int i = 0; i < getraenkeList.Count; i++)
            {
                Console.WriteLine("Das Getränk " + getraenkeList[i].Name + " gibt es insgesamt " + getraenkeList[i].Anzahl + " mal, im Lagerort " + getraenkeList[i].Lagerort + ".");
            }
        }

    }
}
