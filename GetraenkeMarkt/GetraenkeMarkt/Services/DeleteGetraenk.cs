﻿using GetraenkeMarkt.Classes;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace GetraenkeMarkt.Services
{
    public class DeleteGetraenke : ShowDrinks
    {
        public List<Getraenke> DeleteDrinks(List<Getraenke> getraenkeListe)
        {
            Console.Clear();
            Console.WriteLine("Diese Getränke sind momentan erfasst:");
            ShowGetraenke(getraenkeListe);

            Console.WriteLine("Welches Getränk würden Sie denn gerne löschen?\n" +
                "Geben Sie den Namen des Getränks ein: ");
            var input = Console.ReadLine();

            var toDeleteItem = getraenkeListe.Find(x => x.Name.Contains(input));
            getraenkeListe.Remove(toDeleteItem);

            return getraenkeListe;
        }
    }
}
