﻿using GetraenkeMarkt.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GetraenkeMarkt.Services
{
    public class AddDrink
    {
        private static string GetraenkeName = "Was ist der Name des Getränks? ";
        private static string GetraenkeAnzahl = "Wie groß ist die Anzahl der Getränke? ";
        private static string GetraenkeLagerort= "Wo werden die Getränke gelagert? \n" +
            "Wählen Sie zwischen folgenden Möglichkeiten: \n" +
            "1. Lagerbereich A\n" +
            "2. Lagerbereich B\n" +
            "3. Lagerbereich C\n";
        
        public List<Getraenke> CreateAndAddDrink(List<Getraenke> getraenkeListe)
        {
            Console.Clear();
            Getraenke getraenk = new Getraenke();

            getraenk.Name = GetName();
            getraenk.Anzahl = GetAnzahl();
            getraenk.Lagerort = GetLagerort();
            getraenkeListe.Add(getraenk);
            Console.Clear();

            return getraenkeListe;
        }

        private string GetName()
        {
            Console.WriteLine(GetraenkeName);
            var input = Console.ReadLine();

            return input;
        }

        private string GetAnzahl()
        {
            Console.WriteLine(GetraenkeAnzahl);
            var input = Console.ReadLine();

            return input;
        }

        private string GetLagerort()
        {
            Console.WriteLine(GetraenkeLagerort);
            var input = Console.ReadLine();

            return input;
        }
    }
}
